﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    class Program {
        static void Main(string[] args) {
            string[] cores = { "Preto", "Branco", "Verde", "Vermelho", "Azul" };

            //var resultado = cores.Where(x => x.Contains("e") && x.Contains("z")).OrderByDescending(y => y); // exemplo de "and" => "&&"
            var resultado = cores.Where(x => x.Contains("e") || x.Contains("z")).OrderByDescending(y => y); // exemplo de "or" => "||"

            foreach (var cor in resultado) {
                Console.WriteLine(cor);
            }

            Console.ReadKey();
        }
    }
}
