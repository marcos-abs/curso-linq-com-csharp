﻿using Loja.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace Loja {
    class Program {
        static void Main(string[] args) {

            var produtosFrutas = new Produto().ListarFrutas();
            var produtosEletronicos = new Produto().ListarEletronicos();
            var produtos = new List<Produto>();

            produtos.AddRange(produtosFrutas);
            produtos.AddRange(produtosEletronicos);

            var valorProdutoMaisCaro = produtos.Max(x => x.Valor);
            var valorProdutoMaisBarato = produtos.Min(x => x.Valor);
            var mediaValor = produtos.Average(x => x.Valor);
            var somatorioValor = produtos.Sum(x => x.Valor);

            Console.WriteLine("Produto mais caro é: " + valorProdutoMaisCaro.ToString("F2", CultureInfo.InvariantCulture));
            Console.WriteLine("Produto mais barato é: " + valorProdutoMaisBarato.ToString("F2", CultureInfo.InvariantCulture));
            Console.WriteLine("Média do valor é: " + mediaValor.ToString("F2", CultureInfo.InvariantCulture));
            Console.WriteLine("Somatório do valor é: " +  somatorioValor.ToString("F2", CultureInfo.InvariantCulture));

            Console.WriteLine("---------------------------------------------------");

            produtos.ForEach(x => {
                Console.WriteLine(JsonConvert.SerializeObject(x));
            });

            Console.WriteLine("---------------------------------------------------");

            var resultado = (from p in produtos
                            group p by p.Categoria into grupo
                            select new RelatorioProdutoPorCategoria {
                                NomeDaCategoria = grupo.Key,
                                ValorMinimo = grupo.Min(x => x.Valor),
                                ValorMaximo = grupo.Max(x => x.Valor),
                                ValorTotal = grupo.Sum(x => x.Valor)
                            }).OrderBy(x=>x.NomeDaCategoria);

            //foreach (var item in resultado) {
            //    Console.WriteLine("\nNome Categoria: {0} Minimo: {1} Maximo: {2}", item.NomeDaCategoria, item.ValorMinimo, item.ValorMaximo);
            //}

            resultado.ToList().ForEach(x => {
                Console.WriteLine(JsonConvert.SerializeObject(x));
            });

            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("\nPressione qualquer tecla para terminar.");
            Console.ReadKey();
        }
    }
    public class RelatorioProdutoPorCategoria {
        public string NomeDaCategoria { get; set; }
        public decimal ValorMinimo { get; set; }
        public decimal ValorMaximo { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
