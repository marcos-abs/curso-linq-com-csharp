﻿using System;
using System.Collections.Generic;

namespace Loja.Dominio.Entidades {
    public class Produto {

        private List<Produto> _produtos;

        public Produto() {
            _produtos = new List<Produto>();
        }

        public List<Produto> ListarFrutas() {
            _produtos.Add(new Produto() { Nome = "Banana", Valor = 2, Categoria = "Frutas" });
            _produtos.Add(new Produto() { Nome = "Morango", Valor = 25, Categoria = "Frutas" });
            _produtos.Add(new Produto() { Nome = "Pera", Valor = 12, Categoria = "Frutas" });
            _produtos.Add(new Produto() { Nome = "Abacaxi", Valor = 4, Categoria = "Frutas" });
            _produtos.Add(new Produto() { Nome = "Melão", Valor = 5, Categoria = "Frutas" });
            return _produtos;
        }

        public List<Produto> ListarEletronicos() {
            _produtos.Add(new Produto() { Nome = "iPhone X", Valor = 7900, Categoria = "Eletronicos" });
            _produtos.Add(new Produto() { Nome = "Galaxy S7 Edge", Valor = 2500, Categoria = "Eletronicos" });
            _produtos.Add(new Produto() { Nome = "Pendrive", Valor = 100, Categoria = "Eletronicos" });

            return _produtos;
        }

        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public string Categoria { get; set; }
    }
}
