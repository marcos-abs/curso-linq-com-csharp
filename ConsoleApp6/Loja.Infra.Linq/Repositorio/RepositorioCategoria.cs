﻿using System.Collections.Generic;
using System.Linq;

namespace Loja.Infra.Linq.Repositorio {
    public class RepositorioCategoria {
        public void AdicionarCategoria(int id, string nome) {
            // Linq to SQL
            Categoria categoria = new Categoria();
            categoria.id = id;
            categoria.Nome = nome;

            LojaLinqToSQLDataContext linq = new LojaLinqToSQLDataContext();
            linq.Categorias.InsertOnSubmit(categoria);  // INSERT INTO ...
            linq.SubmitChanges();                       // COMMIT ...
        }

        public void AlterarCategoria(int id, string nome) {
            // Linq to SQL
            LojaLinqToSQLDataContext linq = new LojaLinqToSQLDataContext();

            var categoria = linq.Categorias.First(x => x.id == id);

            categoria.Nome = nome;

            linq.SubmitChanges();                       // COMMIT ...
        }

        public void ExcluirCategoria(int id) {
            // Linq to SQL
            LojaLinqToSQLDataContext linq = new LojaLinqToSQLDataContext();

            var categoria = linq.Categorias.First(x => x.id == id);

            linq.Categorias.DeleteOnSubmit(categoria);  // DELETE ...

            linq.SubmitChanges();                       // COMMIT ...
        }

        public List<Produto> ListarProdutos() {
            // Linq to SQL
            LojaLinqToSQLDataContext linq = new LojaLinqToSQLDataContext();
            //return linq.Produtos.ToList(); // Exemplo normal - sem paralelismo
            return linq.Produtos.AsParallel().ToList(); // Exemplo com paralelismo (mais rapido em determinadas operações (principalmente banco de dados).
        }
    }
}
