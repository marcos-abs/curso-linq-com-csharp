﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Loja.Infra.EF.Repositorio {
    public class RepositorioCategoria {
        public void AdicionarCategoria(int id, string nome) {
            // Entity Framework
            Categoria categoria = new Categoria();
            categoria.id = id;
            categoria.Nome = nome;

            LojaEFEntities ef = new LojaEFEntities();
            ef.Categoria.Add(categoria); // INSERT INTO ...
            ef.SaveChanges();            // COMMIT ...
        }

        public void AlterarCategoria(int id, string nome) {
            // Entity Framework
            LojaEFEntities ef = new LojaEFEntities();

            var categoria = ef.Categoria.First(x => x.id == id);
            categoria.Nome = nome; // UPDATE FROM ...

            ef.SaveChanges();            // COMMIT ...
        }

        public void ExcluirCategoria(int id) {
            // Entity Framework
            LojaEFEntities ef = new LojaEFEntities();

            var categoria = ef.Categoria.First(x => x.id == id);

            ef.Categoria.Remove(categoria); // DELETE ...

            ef.SaveChanges();            // COMMIT ...
        }

        public List<Produto> ListarProdutos() {
            // Entity Framework
            LojaEFEntities ef = new LojaEFEntities();
            //return ef.Produto.ToList(); // Exemplo normal - sem paralelismo
            //return ef.Produto.AsParallel().ToList(); // Exemplo com paralelismo (mais rapido em determinadas operações (principalmente banco de dados)
            return ef.Produto.AsNoTracking().ToList(); // Exemplo sem rastreamento do objeto (usar somente em leituras) mais rápido.
        }
    }
}
