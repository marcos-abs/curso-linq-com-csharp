﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Loja {
    class Program {
        static void Main(string[] args) {

            #region Exemplo de banco de dados
            // Salvando categoria com o Entity Framework
            //new Infra.EF.Repositorio.RepositorioCategoria().AdicionarCategoria(1, "Frutas");
            //new Infra.EF.Repositorio.RepositorioCategoria().AdicionarCategoria(2, "Eletrônicos");

            // Salvando caterogia com o Linq to SQL
            //new Infra.Linq.Repositorio.RepositorioCategoria().AdicionarCategoria(3, "Roupas");
            //new Infra.Linq.Repositorio.RepositorioCategoria().AdicionarCategoria(4, "Carnes");

            // Alterando categoria com o Entity Framework
            //new Infra.EF.Repositorio.RepositorioCategoria().AlterarCategoria(4, "Papelaria");

            // Alterando caterogia com o Linq to SQL
            //new Infra.Linq.Repositorio.RepositorioCategoria().AlterarCategoria(1, "Frutas vermelhas");
            //new Infra.Linq.Repositorio.RepositorioCategoria().AlterarCategoria(4, "Carnes vermelhas");

            //new Infra.EF.Repositorio.RepositorioCategoria().AdicionarCategoria(5, "Teste 1");
            //new Infra.Linq.Repositorio.RepositorioCategoria().AdicionarCategoria(6, "Teste 2");

            //new Infra.EF.Repositorio.RepositorioCategoria().ExcluirCategoria(5);
            //new Infra.Linq.Repositorio.RepositorioCategoria().ExcluirCategoria(6);

            //var produtosEF = new Infra.EF.Repositorio.RepositorioCategoria().ListarProdutos();
            ////produtosEF.ForEach(x => Console.WriteLine(JsonConvert.SerializeObject(x))); // Erro Newtonsoft.Json.JsonSerializationException: Self referencing loop detected 
            //produtosEF.ForEach(x => Console.WriteLine(x.Nome));

            //var produtosLinq = new Infra.Linq.Repositorio.RepositorioCategoria().ListarProdutos();
            ////produtosEF.ForEach(x => Console.WriteLine(JsonConvert.SerializeObject(x))); // Erro Newtonsoft.Json.JsonSerializationException: Self referencing loop detected 
            //produtosLinq.ForEach(x => Console.WriteLine(x.Nome));

            #endregion

            #region Exemplo de paralelismo
            //try {
            //    int qtde = 1000000;
            //    string tempoProcessamentoNormal = ProcessamentoNormal(qtde);
            //    string tempoProcessamentoParalelo = ProcessamentoParalelo(qtde);

            //    Console.WriteLine("\nTempo processamento paralelo: " + tempoProcessamentoParalelo);
            //    Console.WriteLine("Tempo processamento normal: " + tempoProcessamentoNormal);

            //    Console.WriteLine("\nPressione qualquer tecla para continuar...");
            //    Console.ReadKey();
            //} catch (Exception e) {
            //    Console.WriteLine("Exception: " + e.Message);
            //}
            #endregion

            #region Exemplo de paralelismo com banco de dados (gravando registros)
            //try {
            //    int qtde = 1000;
            //    string tempoProcessamentoNormal = ProcessamentoNormal(qtde);
            //    string tempoProcessamentoParalelo = ProcessamentoParalelo(qtde);

            //    Console.WriteLine("\nTempo processamento paralelo: " + tempoProcessamentoParalelo);
            //    Console.WriteLine("Tempo processamento normal: " + tempoProcessamentoNormal);

            //    Console.WriteLine("\nPressione qualquer tecla para continuar...");
            //    Console.ReadKey();
            //} catch (Exception e) {
            //    Console.WriteLine("Exception: " + e.Message);
            //} 
            #endregion

            #region Exemplo de paralelismo com banco de dados (apagando registros)
            try {
                int qtde = 1000;
                string tempoProcessamentoNormal = RemovendoNormal(qtde);
                string tempoProcessamentoParalelo = RemovendoParalelo(qtde);

                Console.WriteLine("\nTempo processamento paralelo: " + tempoProcessamentoParalelo);
                Console.WriteLine("Tempo processamento normal: " + tempoProcessamentoNormal);

                Console.WriteLine("\nPressione qualquer tecla para continuar...");
                Console.ReadKey();
            } catch (Exception e) {
                Console.WriteLine("Exception: " + e.Message);
            }

            #endregion

            Console.WriteLine("\nPressione qualquer tecla...");
            Console.ReadKey();
        }

        #region Exemplos de metodos de Processamento normal e paralelo
        //private static string ProcessamentoNormal(int qtde) {
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    for (int i = 0; i < qtde; i++) {
        //        Console.WriteLine("Escrevendo (normal) a linha: " + i.ToString());
        //    }

        //    sw.Stop();

        //    return sw.Elapsed.ToString();
        //}

        //private static string ProcessamentoParalelo(int qtde) {
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    Parallel.For(0, qtde, index => {
        //        Console.WriteLine("Escrevendo (paralelo) a linha: " + index.ToString());
        //    });

        //    sw.Stop();

        //    return sw.Elapsed.ToString();
        //} 
        #endregion

        private static string ProcessamentoNormal(int qtde) {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 10; i < qtde+10; i++) {
                // Salvando categoria com o Entity Framework
                new Infra.EF.Repositorio.RepositorioCategoria().AdicionarCategoria(i, "Teste no." + i.ToString());

                Console.WriteLine("Escrevendo SQL com EF (normal) a linha: " + i.ToString());
            }
            for (int i = qtde; i < qtde*2+10; i++) {
                // Salvando caterogia com o Linq to SQL
                new Infra.Linq.Repositorio.RepositorioCategoria().AdicionarCategoria(i, "Teste no." + i.ToString());

                Console.WriteLine("Escrevendo SQL com Linq (normal) a linha: " + i.ToString());
            }

            sw.Stop();

            return sw.Elapsed.ToString();
        }

        private static string ProcessamentoParalelo(int qtde) {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Parallel.For(qtde * 2, qtde * 3 + 10, index => {
                // Salvando categoria com o Entity Framework
                new Infra.EF.Repositorio.RepositorioCategoria().AdicionarCategoria(index, "Teste no." + index.ToString());

                Console.WriteLine("Escrevendo SQL com EF (paralelo) a linha: " + index.ToString());
            });
            Parallel.For(qtde * 3, qtde * 4+10, index => {
                // Salvando caterogia com o Linq to SQL
                new Infra.Linq.Repositorio.RepositorioCategoria().AdicionarCategoria(index, "Teste no." + index.ToString());

                Console.WriteLine("Escrevendo SQL com Linq (normal) a linha: " + index.ToString());
            });

            sw.Stop();

            return sw.Elapsed.ToString();
        }

        private static string RemovendoNormal(int qtde) {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 10; i < qtde + 10; i++) {
                // Salvando categoria com o Entity Framework
                new Infra.EF.Repositorio.RepositorioCategoria().ExcluirCategoria(i);

                Console.WriteLine("Apagando SQL com EF (normal) a linha: " + i.ToString());
            }
            for (int i = qtde; i < qtde * 2 + 10; i++) {
                // Salvando caterogia com o Linq to SQL
                new Infra.Linq.Repositorio.RepositorioCategoria().ExcluirCategoria(i);

                Console.WriteLine("Apagando SQL com Linq (normal) a linha: " + i.ToString());
            }

            sw.Stop();

            return sw.Elapsed.ToString();
        }

        private static string RemovendoParalelo(int qtde) {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Parallel.For(qtde * 2, qtde * 3 + 10, index => {
                // Salvando categoria com o Entity Framework
                new Infra.EF.Repositorio.RepositorioCategoria().ExcluirCategoria(index);

                Console.WriteLine("Apagando SQL com EF (paralelo) a linha: " + index.ToString());
            });
            Parallel.For(qtde * 3, qtde * 4 + 10, index => {
                // Salvando caterogia com o Linq to SQL
                new Infra.Linq.Repositorio.RepositorioCategoria().ExcluirCategoria(index);

                Console.WriteLine("Apagando SQL com Linq (normal) a linha: " + index.ToString());
            });

            sw.Stop();

            return sw.Elapsed.ToString();
        }
    }
}
