﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2 {
    class Program {
        static void Main(string[] args) {
            int[] numeros = { 1, 2, 3, 7, 8, 4, 5, 6, 9, 10 };

            // "select campo from table orderby campo;"

            var resultado = from num in numeros
                            where num > 4
                            orderby num
                            select num;

            var resultado2 = numeros.Where(n => n > 4).OrderBy(x => x);

            foreach (var numero in resultado2) {
                Console.WriteLine(numero);
            }
            Console.ReadKey();
        }
    }
}
