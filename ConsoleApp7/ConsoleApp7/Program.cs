﻿using ConsoleApp7.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CursoLinq {
    public class Program {
        static void Main(string[] args) {

            #region Exemplo comum do uso do Entity Framework
            //LojaContext ef = new LojaContext();
            //var produtos = ef.Produtoes.ToList(); // exemplo ruim pq busca todo o banco de dados para a memoria - ver IQueryable - economia de recursos de banco.
            #endregion

            #region Exemplo com liberação de memoria após a utilização do objeto.
            List<Produto> produtos = new List<Produto>();
            using (LojaContext ef = new LojaContext()) {
                produtos = ef.Produtoes.ToList();
            }
            #endregion

            #region Exemplo impressão comum.
            produtos.ForEach(x => {
                Console.WriteLine(x.Nome);
            });
            #endregion

            Console.WriteLine("---------------------------");

            //skip e take
            var produtosPaginados = produtos.Take(2).ToList();
            produtosPaginados.ForEach(x => {
                Console.WriteLine(x.Nome);
            });

            Console.WriteLine("---------------------------");

            var produtosPaginados2 = produtos.Skip(1).Take(2).ToList();
            produtosPaginados2.ForEach(x => {
                Console.WriteLine(x.Nome);
            });

            // count
            int qtdeProdutos = produtos.Count();
            int qtdeProdutosComLetraA = produtos.Count(x => x.Nome.Contains("a"));
            //Console.WriteLine("\nQuantidade de produtos {0} e somente com a Letra \"A\" {1}", qtdeProdutos, qtdeProdutosComLetraA); // forma comum
            Console.WriteLine($"\nQuantidade de produtos {qtdeProdutos} e somente com a Letra \"A\" {qtdeProdutosComLetraA}"); // interpolation


            // Single - retorna o registro unico, caso tenha mais de uma ocorrência retorna uma exceção.
            Produto produto3 = produtos.Single(x => x.Nome.Contains("x")); // erro de excepção por não encontrar um único registro.
            // SingleOrDefault - retorna o registro unico, caso tenha mais de uma ocorrência retorna nulo.
            Produto produto4 = produtos.SingleOrDefault(x => x.Nome.Contains("w"));
            Console.WriteLine($"\nUnico registro com a letra \"x\"=\"{produto3.Nome}\" e com a Letra \"w\"=\"{produto4?.Nome}\""); // observe o caracter de interrogação depois do nome da variável que recebe o valor nulo.

            Console.WriteLine("\nProdutos menos caros =< 1000,00 reais");

            // SkipWhile
            var produtosMenosCaros = produtos.SkipWhile(x => x.Valor > 1000).ToList();
            produtosMenosCaros.ForEach(x => {
                Console.WriteLine($"Nome: {x.Nome} Valor: {string.Concat("R$", x.Valor, ",00")}");
            });

            // Sum, Max, Min
            var resultadoSum = produtos.Where(x=>x.Valor > 1000).Sum(x => x.Valor);
            var resultadoMax = produtos.Max(x => x.Valor);
            var resultadoMin = produtos.Min(x => x.Valor);
            Console.WriteLine($"\nSomatório dos produtos com valor maior que R$ 1000,00={resultadoSum},  Maior valor={resultadoMax} e o Menor valor={resultadoMin}");

            // Union
            Console.WriteLine("\nUnião de vetores");
            int[] vet1 = { 1, 3, 5, 7, 9, 2, 4 };
            int[] vet2 = { 2, 4, 6, 8, 0, 1, 3 };
            IEnumerable<int> union = vet1.Union(vet2).OrderBy(x => x); // elimina os repetidos e ordena de forma crescente.
            union.ToList().ForEach(num => {
                Console.Write("{0} ", num);
            });

            // Distinct
            Console.WriteLine("\n\nDistinct de valores");
            int[] vet = { 5, 3, 9, 7, 5, 3, 9, 7 };
            var vetDistinctArray = vet.Distinct(); // o Distinct selecione um único valor de cada repetição
            vetDistinctArray.ToList().ForEach(num => {
                Console.Write($"Numero: {num} ");
            });

            Console.WriteLine("\n\nDistinct de objetos");
            var iphones = produtos.Where(x => x.Nome.Contains("iPhone")).Select(x => x.Nome).Distinct().ToList(); // o Distinct selecione um único valor (sem repetições)
            iphones.ForEach(nome => {
                Console.WriteLine($"Produto: {nome}");
            });

            // Any
            var existe = produtos.Any(); // Existem registros? retorna verdadeiro.
            var existeLivros = produtos.Any(x => x.Nome == "Livro"); // Existem registros com o nome Livro? retorna falso.
            Console.WriteLine($"\n\nTem produtos?{(existe?"Sim":"Não")} e Livros?{(existeLivros? "Sim" : "Não")}"); // com condições ternárias.

            // IQueryable - economia de recursos de banco de dados.
            Console.WriteLine("\n\nIQueryable");
            int? valor = 1000; // condição1 para o teste do exemplo
            string temLetra = "o"; // condição2 para o teste do exemplo
            using (LojaContext ef = new LojaContext()) {
                var queryRuim = ef.Produtoes.ToList(); // exemplo ruim, lê todo o banco de dados.
                var queryLegal = ef.Produtoes; // exemplo bom, inicia a montagem da query sem ler todo o banco de dados.
                if (valor.HasValue) {
                    var produtosValorMaior = queryLegal.Where(x => x.Valor > valor).ToList(); // acrescenta novo filtro na query ainda sem ler o banco.
                }
                if (!string.IsNullOrEmpty(temLetra)) {
                    queryLegal.Where(x => x.Nome.Contains(temLetra)); // acrescenta outro novo filtro na query ainda sem ler o banco.
                }
                produtos = queryLegal.ToList(); // agora a query resulta num select no banco.
                produtos.ForEach(item => {
                    Console.WriteLine($"Produto: {item.Nome} Valor: {item.Valor}");
                });
            }

            Console.WriteLine("\n\nPressione qualquer tecla para encerrar.");
            Console.ReadKey();
        }
    }
}
